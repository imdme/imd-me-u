from fastapi import APIRouter, HTTPException
from typing import List
import requests

router = APIRouter()

API_URL = (
    "https://api.themoviedb.org/3/search/movie?"
    "api_key=ade9ac2663bdc8bc0eae7b07d7787d12&query="
)


@router.get("/movies/{movie_id}/videos")
async def get_movie_videos(movie_id: int) -> dict:
    try:
        url = (
            f"https://api.themoviedb.org/3/movie/{movie_id}"
            "/videos?api_key=ade9ac2663bdc8bc0eae7b07d7787d12"
        )
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise HTTPException(
                status_code=response.status_code, detail=response.json()
            )

        return response.json()

    except requests.exceptions.HTTPError as e:
        raise HTTPException(status_code=response.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/movies/{movie_id}/watch-providers")
async def get_movie_watch_providers(movie_id: int) -> dict:
    try:
        url = (
            f"https://api.themoviedb.org/3/movie/{movie_id}"
            "/watch/providers?api_key=ade9ac2663bdc8bc0eae7b07d7787d12"
        )
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise HTTPException(
                status_code=response.status_code, detail=response.json()
            )

        return response.json()

    except requests.exceptions.HTTPError as e:
        raise HTTPException(status_code=response.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/movies/{movie_name}")
async def get_movies(movie_name: str) -> List[dict]:
    try:
        url = API_URL + movie_name
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise HTTPException(
                status_code=response.status_code, detail=response.json()
            )

        results = response.json()["results"]
        return results

    except requests.exceptions.HTTPError as e:
        raise HTTPException(status_code=response.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/popular/")
def get_popular_movies():
    API_URL = "https://api.themoviedb.org/3"
    API_KEY = "ade9ac2663bdc8bc0eae7b07d7787d12"
    try:
        url = f"{API_URL}/movie/popular?api_key={API_KEY}"
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise Exception(response.json())

        results = response.json()["results"]
        return results

    except Exception as e:
        raise Exception(str(e))


@router.get("/movies/{movie_id}/detail")
async def get_movie_detail(movie_id: int) -> dict:
    try:
        url = (
            f"https://api.themoviedb.org/3/movie/{movie_id}"
            "?api_key=ade9ac2663bdc8bc0eae7b07d7787d12"
        )
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise HTTPException(
                status_code=response.status_code, detail=response.json()
            )

        return response.json()

    except requests.exceptions.HTTPError as e:
        raise HTTPException(status_code=response.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/movies/{movie_id}/reviews")
async def get_movie_reviews(movie_id: int):
    try:
        url = (
            f"https://api.themoviedb.org/3/movie/{movie_id}/reviews"
            "?api_key=ade9ac2663bdc8bc0eae7b07d7787d12"
        )
        response = requests.get(url)

        if response.status_code < 200 or response.status_code >= 300:
            raise HTTPException(
                status_code=response.status_code, detail=response.json()
            )

        return response.json()

    except requests.exceptions.HTTPError as e:
        raise HTTPException(status_code=response.status_code, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
